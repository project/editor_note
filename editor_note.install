<?php
/**
 * @file
 * Database schema and installations tasks for Editor Notes module.
 */

/**
 * Implements hook_schema().
 */
function editor_note_schema() {
  $schema['editor_note'] = array(
    'description' => 'Base table for editor notes.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: unique note id.',
      ),
      'note' => array(
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'description' => 'Content of the note.',
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The entity type note is attached to.',
      ),
      'bundle' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The bundle of the entity note is attached to.',
      ),
      'field_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The {field_config}.field name of the field containing editor note.',
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id note is attached to.',
      ),
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The entity revision id note is attached to, or NULL if the entity type is not versioned.',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid who authored the note.',
      ),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The time that the note was created, as a Unix timestamp.',
      ),
      'changed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The time that the note was last edited, as a Unix timestamp.',
      ),
      'text_format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The text format of an editor note.',
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'editor_note_id' => array('id'),
      'editor_note_uid' => array('uid'),
      'editor_note_changed' => array('changed'),
      'editor_note_created' => array('created'),
    ),
  );

  return $schema;
}

/**
 * Adds 'text_format' database field to support text formats.
 */
function editor_note_update_7001() {
  $table = 'editor_note';
  $field_name = 'text_format';

  if (!db_field_exists($table, $field_name)) {
    $field = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
      'description' => 'The text format of an editor note.',
    );
    db_add_field($table, $field_name, $field);
  }
}

/**
 * Updates blank text_format to the default 'plain_text' format.
 */
function editor_note_update_7002() {
  $blank_text_format_notes = db_query('SELECT en.id FROM {editor_note} en WHERE text_format = :text_format',
    array(':text_format' => ''))->fetchAll();

  foreach ($blank_text_format_notes as $item) {
    $ids[] = $item->id;
  }

  if (!empty($ids)) {
    db_update('editor_note')
      ->fields(array('text_format' => EDITOR_NOTE_DEFAULT_TEXT_FORMAT))
      ->condition('id', $ids, 'IN')
      ->execute();
  }
}
